## Synopsis


This code is used to deploy a Jupyter notebook server 
packaged as a Singularity container to an Ubuntu host. 
 
It should be run from the host on which you want it deployed, 
using an ansible playbook to invoke the shell script
````
    install.sh
````


## Usage

```
./install.sh
```
