#!/bin/bash
#
# Install Singularity
#

DEBIAN_FRONTEND=noninteractive
WORKING_DIR=/srv/singularity-images
sudo apt-get update
sudo apt-get install -yq --no-install-recommends \
     apt-transport-https \
     build-essential \
     bzip2 \
     ca-certificates \
     curl \
     golang-1.16-go \
     httpie \
     libarchive-dev \
     libgpgme11-dev \
     libjpeg-dev \
     libpng-dev \
     libseccomp-dev \
     libssl-dev \
     locales \
     net-tools \
     pkg-config
     pwgen \
     software-properties-common \
     squashfs-tools \
     unzip \
     uuid-dev \
     wget \
     zlib1g-dev
sudo apt-get clean
sudo rm -rf /var/lib/apt/lists/*
sudo apt-get autoremove -y
sudo update-alternatives --install /usr/bin/go go /usr/lib/go-1.16/bin/go 0

sudo chgrp $(id -g ${USER}) -R /srv
sudo chmod g+rw /srv
if [ -d "$WORKING_DIR" ]; then rm -Rf $WORKING_DIR; fi
mkdir -p $WORKING_DIR

# Install a recent Singularity version
# This is done in a subshell to keep the working directory neutral
( sudo go get -u github.com/golang/dep/cmd/dep
export VERSION=3.8.1 && # adjust this as necessary \
    sudo mkdir -p /usr/local/src/github.com/sylabs && \
    cd /usr/local/src/github.com/sylabs && \
    sudo wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \
    sudo tar -xzf singularity-ce-${VERSION}.tar.gz && \
    cd ./singularity-ce-${VERSION} && \
    sudo ./mconfig && \
    sudo make -C ./builddir && \
    sudo make -C ./builddir install
)

# need AllowTcpForwarding to be true for ssh port forwarding to work
sudo sed -i 's/#AllowTcpForwarding yes/AllowTcpForwarding yes/' /etc/ssh/sshd_config
sudo service ssh restart

# grab the pre-built singularity image
singularity pull \
     $WORKING_DIR/singularity-jupyter.sif \
     oras://gitlab-registry.oit.duke.edu/devil-ops/singularity-jupyter/singularity-jupyter:0.0.1

# put the source somewhere so users can modify things after the deploy
git clone https://gitlab.oit.duke.edu/devil-ops/ansible-rapid-jupyter.git $WORKING_DIR/src/deploy

git clone https://gitlab.oit.duke.edu/devil-ops/singularity-jupyter.git $WORKING_DIR/src/singularity-jupyter
cp $WORKING_DIR/src/singularity-jupyter/jupyter_notebook_config.py $WORKING_DIR/
cp $WORKING_DIR/src/singularity-jupyter/run-jupyterlab $WORKING_DIR/
cp $WORKING_DIR/src/singularity-jupyter/findport-jupyterlab $WORKING_DIR/

# drop off some documentation in skel for use in users homedir
sudo cp -R $WORKING_DIR/src/deploy/singularity-user-docs/* /etc/skel/
sudo cp $WORKING_DIR/{run-jupyterlab,findport-jupyterlab} /etc/skel/
sudo mkdir -p /etc/skel/.jupyter/
sudo cp $WORKING_DIR/jupyter_notebook_config.py /etc/skel/.jupyter/

# setup this beautiful homedir for existing users we care about
./update_existing_users.sh

#
# leave a flag saying we got done
#
echo "Jupyter app install succeeded" > /tmp/rapid_image_complete
