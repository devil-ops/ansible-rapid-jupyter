#!/bin/bash
#
getent passwd |
  while IFS=: read username x uid gid gecos home shell
  do
    [[ "$username" != vcm && "$username" != rapiduser || ! -d "$home" ]] && continue
    tar -cf - -C /etc/skel . | sudo -Hu "$username" tar --skip-old-files -xf - -C "/home/$username"
  done
