JupyterLab is packaged in a Singularity container so that the binary is easily
portable to any platform that has Singularity installed.

To run an instance of JupyterLab, run these commands

     cd ~ 
     ./run-jupyterlab

then follow the instructions displayed once the run-jupyterlab script starts
to enable ssh port forwarding to access your container.

Once the container is running you will have access to files in your home directory
and a tmp directory on the server from your web browser, so you should place your data files
in your home directory.

=========
If you want to modify the JupyterLab container, a local copy of the Singularity 
build script is stored on this server here:

   /srv/singularity-images/src/singularity-jupyter

There are build scripts included in addition to some documentation on the
expectations of the process in that directory as well.